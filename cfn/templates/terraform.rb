SparkleFormation.new(:terraform) do
  resources do
    bucket do
      Type 'AWS::S3::Bucket'
      Properties do
        AccessControl 'Private'
        BucketEncryption do
          ServerSideEncryptionConfiguration [{ ServerSideEncryptionByDefault: { SSEAlgorithm: 'AES256' } }]
        end
      end
    end
  end

  outputs do
    bucket { Value ref!(:bucket) }
  end
end
