SparkleFormation.new(:hosted_zones) do
  zones = {
    'alexandmarissa_dot_com' => 'alexandmarissa.com.',
    'alexrecker_dot_com' => 'alexrecker.com.',
    'astuaryart_dot_com' => 'astuaryart.com.',
    'bobrosssearch_dot_com' => 'bobrosssearch.com.',
    'fromdirktolight_dot_com' => 'fromdirktolight.com.',
    'reckerfamily_dot_com' => 'reckerfamily.com.',
    'sarahrecker_dot_com' => 'sarahrecker.com.',
    'tranquilitydesignsmn_dot_com' => 'tranquilitydesignsmn.com'
  }
  
  resources do
    zones.each do |resource, name|
      set!(resource) do
        type 'AWS::Route53::HostedZone'
        deletion_policy 'Retain'
        properties do
          name name
        end
      end
    end
  end
end
