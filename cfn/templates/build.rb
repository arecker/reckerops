SparkleFormation.new(:build) do
  resources do
    vpc do
      type 'AWS::EC2::VPC'
      properties do
        cidr_block '10.0.0.0/24'
        enable_dns_support true
        enable_dns_hostnames true
        tags tags!(name: 'build')
      end
    end

    gateway do
      type 'AWS::EC2::InternetGateway'
      properties do
        tags tags!(name: 'build')
      end
    end

    gateway_attachment do
      type 'AWS::EC2::VPCGatewayAttachment'
      properties do
        internet_gateway_id ref!(:gateway)
        vpc_id ref!(:vpc)
      end
    end

    route_table do
      type 'AWS::EC2::RouteTable'
      properties do
        vpc_id ref!(:vpc)
        tags tags!(name: 'build')
      end
    end

    public_route do
      type 'AWS::EC2::Route'
      properties do
        gateway_id ref!(:gateway)
        route_table_id ref!(:route_table)
        destination_cidr_block '0.0.0.0/0'
      end
    end

    subnet do
      type 'AWS::EC2::Subnet'
      properties do
        cidr_block '10.0.0.0/24'
        map_public_ip_on_launch true
        vpc_id ref!(:vpc)
        tags tags!(name: 'build')
      end
    end

    route_table_attachment do
      type 'AWS::EC2::SubnetRouteTableAssociation'
      properties do
        route_table_id ref!(:route_table)
        subnet_id ref!(:subnet)
      end
    end

    security_group do
      type 'AWS::EC2::SecurityGroup'
      properties do
        group_description 'build-default-security-group'
        vpc_id ref!(:vpc)
        security_group_ingress [
          {
            CidrIp: '0.0.0.0/0',
            Description: 'allow-ssh',
            FromPort: '22',
            ToPort:'22',
            IpProtocol: 'tcp'
          },
          {
            CidrIp: '0.0.0.0/0',
            Description: 'allow-http',
            FromPort: '80',
            ToPort:'80',
            IpProtocol: 'tcp'
          },
          {
            CidrIp: '0.0.0.0/0',
            Description: 'allow-https',
            FromPort: '443',
            ToPort:'443',
            IpProtocol: 'tcp'
          },
          {
            CidrIp: '0.0.0.0/0',
            Description: 'allow-icmp',
            FromPort: '-1',
            ToPort:'-1',
            IpProtocol: 'icmp'
          }
        ]
        tags tags!(name: 'build-default')
      end
    end
  end
end
