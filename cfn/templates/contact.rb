SparkleFormation.new(:contact) do
  parameters do
    api_domain_name { type 'String' }
    api_cloudfront_domain_name { type 'String' }
    hosted_zone_name { type 'String' }
    message_body { type 'String' }
    message_subject { type 'String' }
    recipient_address { type 'String' }
    sender_address { type 'String' }
  end

  resources do
    lambda_role do
      type 'AWS::IAM::Role'
      properties do
        assume_role_policy_document do
          version '2012-10-17'
          statement [{ Effect: 'Allow', Action: 'sts:AssumeRole', Principal: { Service: 'lambda.amazonaws.com' } }]
        end
        managed_policy_arns ['arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole']
        policies [
          {
            PolicyName: 'send-email-policy',
            PolicyDocument: {
              Version: '2012-10-17',
              Statement: [
                {
                  Effect: 'Allow',
                  Action: 'ses:SendEmail',
                  Resource: [
                    join!('arn:aws:ses:us-west-2:', account_id!, ':identity/', ref!(:sender_address)),
                    join!('arn:aws:ses:us-west-2:', account_id!, ':identity/', ref!(:recipient_address)),
                  ]
                }
              ]
            }
          }
        ]
      end
    end

    lambda_function do
      type 'AWS::Lambda::Function'
      properties do
        code.zip_file joined_file!('contact_lambda_function.py')
        handler 'index.handler'
        role attr!(:lambda_role, :arn)
        runtime 'python3.6'
        environment.variables do
          message_body ref!(:message_body)
          message_subject ref!(:message_subject)
          recipient_address ref!(:recipient_address)
          sender_address ref!(:sender_address)
        end
      end
    end

    api do
      type 'AWS::ApiGateway::RestApi'
      properties do
        name stack_name!
        endpoint_configuration.types ['EDGE']
      end
    end

    api_resource do
      type 'AWS::ApiGateway::Resource'
      properties do
        parent_id attr!(:api, :root_resource_id)
        path_part 'email'
        rest_api_id ref!(:api)
      end
    end

    api_options_method do
      type 'AWS::ApiGateway::Method'
      properties do
        authorization_type 'NONE'
        http_method 'OPTIONS'
        request_parameters {}
        method_responses [
          {
            StatusCode: '200',
            ResponseModels: {},
            ResponseParameters: {
              "method.response.header.Access-Control-Allow-Origin" => true,
              "method.response.header.Access-Control-Allow-Headers" => true,
              "method.response.header.Access-Control-Allow-Methods" => true,
              "method.response.header.Access-Control-Allow-Credentials" => true
            }
          }
        ]
        integration do
          type 'MOCK'
          request_templates do
            set!('application/json'._no_hump, "{statusCode:200}")
          end
          integration_responses [
            {
              StatusCode: '200',
              ResponseParameters: {
                "method.response.header.Access-Control-Allow-Origin" => "'*'",
                "method.response.header.Access-Control-Allow-Headers" => "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent'",
                "method.response.header.Access-Control-Allow-Methods" => "'OPTIONS,POST'",
                "method.response.header.Access-Control-Allow-Credentials" => "'false'"
              },
              ResponseTemplates: {
                "application/json" => ""
              }
            }
          ]
        end
        resource_id ref!(:api_resource)
        rest_api_id ref!(:api)
      end
    end

    api_post_method do
      type 'AWS::ApiGateway::Method'
      properties do
        http_method 'POST'
        request_parameters {}
        resource_id ref!(:api_resource)
        rest_api_id ref!(:api)
        api_key_required false
        authorization_type 'NONE'
        integration do
          integration_http_method 'POST'
          type 'AWS_PROXY'
          uri join!('arn:aws:apigateway:', region!, ':lambda:path/2015-03-31/functions/', attr!(:lambda_function, :arn), '/invocations')
        end
        method_responses []
      end
    end

    api_deployment do
      depends_on!(:api_post_method)
      type 'AWS::ApiGateway::Deployment'
      properties do
        rest_api_id ref!(:api)
        stage_name 'v1'
      end
    end

    api_lambda_permission do
      type 'AWS::Lambda::Permission'
      properties do
        function_name attr!(:lambda_function, :arn)
        action 'lambda:InvokeFunction'
        principal 'apigateway.amazonaws.com'
        source_arn join!('arn:aws:execute-api:', region!, ':', account_id!, ':', ref!(:api), '/*/*')
      end
    end

    api_mapping do
      depends_on!(:api_deployment)
      type 'AWS::ApiGateway::BasePathMapping'
      properties do
        base_path '(none)'
        domain_name ref!(:api_domain_name)
        rest_api_id ref!(:api)
        stage 'v1'
      end
    end

    dns_record do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name ref!(:hosted_zone_name)
        record_sets [
          {
            Name: ref!(:api_domain_name),
            Type: 'A',
            AliasTarget: {
              DNSName: ref!(:api_cloudfront_domain_name),
              HostedZoneId: 'Z2FDTNDATAQYW2'
            }
          }
        ]
      end
    end
  end
end
