SparkleFormation.new(:dns) do
  resources do
    alexandmarissa_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'alexandmarissa.com.'
        record_sets [
          {
            Name: 'alexandmarissa.com',
            Type: 'MX',
            TTL: '3600',
            ResourceRecords: [
              '10 mx1.privateemail.com',
              '10 mx2.privateemail.com'
            ]
          },
          {
            Name: 'alexandmarissa.com',
            Type: 'TXT',
            TTL: '3600',
            ResourceRecords: [
              "\"v=spf1 include:spf.privateemail.com ~all\"",
            ]
          },
          {
            Name: 'mail.alexandmarissa.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com'
            ]
          },
          {
            Name: 'autodiscover.alexandmarissa.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com'
            ]
          },
          {
            Name: 'autoconfig.alexandmarissa.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com'
            ]
          },
          {
            Name: '_autodiscover._tcp.alexandmarissa.com',
            Type: 'SRV',
            TTL: '60',
            ResourceRecords: [
              '0 0 443 privateemail.com'
            ]
          },
          {
            Name: '_8975bb72445a03cb2eac785db02df900.www.alexandmarissa.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_abbebe22d46fd26c148e84464fd92d94.acm-validations.aws.'
            ]
          },
          {
            Name: '_9b6c62a54140cc353201d576344dc5e9.alexandmarissa.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_63370ee7158160fd2dad2102d377c786.acm-validations.aws.'
            ]
          }
        ]
      end
    end

    astuary_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'astuaryart.com.'
        record_sets [
          {
            Name: 'astuaryart.com',
            Type: 'A',
            TTL: '900',
            ResourceRecords: [
              '198.185.159.144',
              '198.185.159.145',
              '198.49.23.144',
              '198.49.23.145'
            ]
          },
          {
            Name: 'mhchbrbazfjmhxkzrn8c.astuaryart.com',
            Type: 'CNAME',
            TTL: '900',
            ResourceRecords: [
              'verify.squarespace.com'
            ]
          },
          {
            Name: 'www.astuaryart.com',
            Type: 'CNAME',
            TTL: '900',
            ResourceRecords: [
              'ext-cust.squarespace.com'
            ]
          }
        ]
      end
    end

    alex_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'alexrecker.com.'
        record_sets [
          {
            Name: '_51f80347a1363e6bb9cbf7747e20c100.demo.alexrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_e27ab6998cc5d6cfbf6cdd97f8b1f03b.acm-validations.aws.'
            ]
          },
          {
            Name: '_1ec36ef1d8d85f92e9b8e6fba66407d7.alexrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_64b801e8f440524c6bfb33589ce46672.acm-validations.aws.'
            ]
          },
          {
            Name: '_6d3bd331262ba8fa1942cb67364a1c0a.www.alexrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_d6c6054ce0d03e793a4f0b1d59a25eda.acm-validations.aws.'
            ]
          }
        ]
      end
    end

    bob_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'bobrosssearch.com.'
        record_sets [
          {
            Name: '_4c1fa76ab065386305f88408b1c5a110.www.bobrosssearch.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_6568253aebf6762a52c4a3aff694cdfa.acm-validations.aws.'
            ]
          },
          {
            Name: '_73a1a2bde3ada19783c8874703f40178.bobrosssearch.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_ecb5d704f17adb5734597fc095a0f621.acm-validations.aws.'
            ]
          }
        ]
      end
    end

    reckerfamily_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'reckerfamily.com.'
        record_sets [
          {
            Name: 'reckerfamily.com',
            Type: 'MX',
            TTL: '3600',
            ResourceRecords: [
              '1 ASPMX.L.GOOGLE.COM.',
              '5 ALT1.ASPMX.L.GOOGLE.COM.',
              '5 ALT2.ASPMX.L.GOOGLE.COM.',
              '10 ALT3.ASPMX.L.GOOGLE.COM.',
              '10 ALT4.ASPMX.L.GOOGLE.COM.'
            ]
          }
        ]
      end
    end

    tranquility_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'tranquilitydesignsmn.com.'
        record_sets [
          {
            Name: 'tranquilitydesignsmn.com',
            Type: 'MX',
            TTL: '3600',
            ResourceRecords: [
              '10 mx1.privateemail.com',
              '10 mx2.privateemail.com'
            ]
          },
          {
            Name: 'tranquilitydesignsmn.com',
            Type: 'TXT',
            TTL: '3600',
            ResourceRecords: [
              "\"v=spf1 include:spf.privateemail.com ~all\"",
            ]
          },
          {
            Name: 'mail.tranquilitydesignsmn.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com',
            ]
          },
          {
            Name: 'autodiscover.tranquilitydesignsmn.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com',
            ]
          },
          {
            Name: 'autoconfig.tranquilitydesignsmn.com',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              'privateemail.com',
            ]
          },
          {
            Name: '_autodiscover._tcp.tranquilitydesignsmn.com',
            Type: 'SRV',
            TTL: '60',
            ResourceRecords: [
              '0 0 443 privateemail.com',
            ]
          },
          {
            Name: '_558238ef010d11dbe632d392943988d5.tranquilitydesignsmn.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_7316d0699714bf38571ab2e0610cd2f8.acm-validations.aws.'
            ]
          },
          {
            Name: '_e1bf33ddf9b6f9d90fd6812be7437d9e.www.tranquilitydesignsmn.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_66f1c0e425905e885823c1bfa6b38783.acm-validations.aws.'
            ]
          },
          {
            Name: '_5bfba514fa57efad86cc0f606c890464.api.tranquilitydesignsmn.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_e8fb87937c1cf4f5a6fa7dd3097368c3.acm-validations.aws.'
            ]
          }
        ]
      end
    end

    dirk_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'fromdirktolight.com.'
        record_sets [
          {
            Name: '_e979a33ba9a26bb4378b1ed420c9b574.www.fromdirktolight.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_b354d4481e1216fd9cf6a283a4d5d8ef.acm-validations.aws.'
            ]
          },
          {
            Name: '_e06a65824a8d684f9400acb0a4c1aff3.fromdirktolight.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_8b600fdb543c05bf2002a0037ef80f0e.acm-validations.aws.'
            ]
          }
        ]
      end
    end

    sarah_records do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name 'sarahrecker.com.'
        record_sets [
          {
            Name: '_65aa3f939c04fc0ba16c47b7f32380bc.api.sarahrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_4c3d9f0d2edd74694cdcc7c3026ea0c8.acm-validations.aws.'
            ]
          },
          {
            Name: '_8a25dcb0990177b5dc4abc65c00d4aad.blog.sarahrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_0d9318ed6bee8625c5ccf4fc53a77e87.acm-validations.aws.'
            ]
          },
          {
            Name: '_85dbd81e1ba1f2f4787472ded24d2579.www.sarahrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_e971b76034430720efb59876fa9dffb7.acm-validations.aws.'
            ]
          },
          {
            Name: '_a904d2f1c37e9ed523909903104e2f4f.sarahrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_c55fcb6d34cbd7fae0fd2dd3ebe57349.acm-validations.aws.'
            ]
          },
          {
            Name: '_ef5a516a3c3d85fbef582251d326ab25.scrabble.sarahrecker.com.',
            Type: 'CNAME',
            TTL: '60',
            ResourceRecords: [
              '_347230f0b7e6715ad4114a103a51ef5b.acm-validations.aws.'
            ]
          }
        ]
      end
    end
  end
end
