SparkleFormation.new(:site) do
  parameters do
    cert_arn { type 'String' }
    hosted_zone_name { type 'String' }
    hostname { type 'String' }
    
    hostname_redirect do
      type 'String'
      default ''
    end

    create_publisher do
      type 'String'
      default 'false'
      allowed_values ['true', 'false']
    end
  end

  conditions do
    create_redirect not!(equals!(ref!(:hostname_redirect), ''))
    create_publisher equals!(ref!(:create_publisher), 'true')
    dont_create_publisher not!(equals!(ref!(:create_publisher), 'true'))
  end

  resources do
    publisher do
      on_condition!(:create_publisher)
      type 'AWS::IAM::User'
      properties {}
    end
    
    bucket do
      type 'AWS::S3::Bucket'
      properties do
        website_configuration do
          index_document 'index.html'
        end
      end
    end

    bucket_policy do
      on_condition!(:dont_create_publisher)
      type 'AWS::S3::BucketPolicy'
      properties do
        bucket ref!(:bucket)
        policy_document do
          version '2012-10-17'
          statement [
            {
              Action: 's3:GetObject',
              Effect: 'Allow',
              Principal: '*',
              Resource: join!(attr!(:bucket, :arn), '/*')
            }
          ]
        end
      end
    end
    
    publisher_bucket_policy do
      on_condition!(:create_publisher)
      type 'AWS::S3::BucketPolicy'
      properties do
        bucket ref!(:bucket)
        policy_document do
          version '2012-10-17'
          statement [
            {
              Action: ['s3:*'],
              Effect: 'Allow',
              Principal: { AWS: attr!(:publisher, :arn) },
              Resource: [
                attr!(:bucket, :arn),
                join!(attr!(:bucket, :arn), '/*')
              ]
            },
            {
              Action: 's3:GetObject',
              Effect: 'Allow',
              Principal: '*',
              Resource: join!(attr!(:bucket, :arn), '/*')
            }
          ]
        end
      end
    end

    redirect_bucket do
      on_condition!(:create_redirect)
      type 'AWS::S3::Bucket'
      properties do
        website_configuration do
          redirect_all_requests_to do
            host_name ref!(:hostname)
            protocol 'https'
          end
        end
      end
    end

    distribution do
      type 'AWS::CloudFront::Distribution'
      properties do
        distribution_config do
          aliases [ref!(:hostname)]
          default_cache_behavior do
            forwarded_values do
              query_string true
            end
            target_origin_id 'bucket'
            viewer_protocol_policy 'redirect-to-https'
          end
          enabled true
          origins [
            {
              DomainName: select!(2, split!(attr!(:bucket, :WebsiteURL), '/')),
              Id: 'bucket',
              CustomOriginConfig: {
                HTTPPort: '80',
                HTTPSPort: '443',
                OriginProtocolPolicy: 'http-only'
              }
            }
          ]
          price_class 'PriceClass_100'
          viewer_certificate do
            acm_certificate_arn ref!(:cert_arn)
            ssl_support_method 'sni-only'
          end
        end
      end
    end

    redirect_distribution do
      on_condition!(:create_redirect)
      type 'AWS::CloudFront::Distribution'
      properties do
        distribution_config do
          aliases [ref!(:hostname_redirect)]
          default_cache_behavior do
            forwarded_values do
              query_string true
            end
            target_origin_id 'redirect-bucket'
            viewer_protocol_policy 'redirect-to-https'
          end
          enabled true
          origins [
            {
              DomainName: select!(2, split!(attr!(:redirect_bucket, :WebsiteURL), '/')),
              Id: 'redirect-bucket',
              CustomOriginConfig: {
                HTTPPort: '80',
                HTTPSPort: '443',
                OriginProtocolPolicy: 'http-only'
              }
            }
          ]
          price_class 'PriceClass_100'
          viewer_certificate do
            acm_certificate_arn ref!(:cert_arn)
            ssl_support_method 'sni-only'
          end
        end
      end
    end

    dns_record do
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name ref!(:hosted_zone_name)
        record_sets [
          {
            Name: ref!(:hostname),
            Type: 'A',
            AliasTarget: {
              DNSName: attr!(:distribution, :domain_name),
              HostedZoneId: 'Z2FDTNDATAQYW2'
            }
          }
        ]
      end
    end
    
    redirect_dns_record do
      on_condition!(:create_redirect)
      type 'AWS::Route53::RecordSetGroup'
      properties do
        hosted_zone_name ref!(:hosted_zone_name)
        record_sets [
          {
            Name: ref!(:hostname_redirect),
            Type: 'A',
            AliasTarget: {
              DNSName: attr!(:redirect_distribution, :domain_name),
              HostedZoneId: 'Z2FDTNDATAQYW2'
            }
          }
        ]
      end
    end
  end

  outputs do
    bucket { value ref!(:bucket) }
    publisher { value if!(:create_publisher, ref!(:publisher), '<none>') }
  end
end
