SparkleFormation.new(:ddns) do
  parameters do
    hosted_zone_id { type 'String' }
  end

  resources do
    user do
      type 'AWS::IAM::User'
      properties do
        user_name join!(stack_name!, '-user')
        policies [
          {
            PolicyName: join!(stack_name!, '-policy'),
            PolicyDocument: {
              Version: '2012-10-17',
              Statement: [
                {
                  Effect: 'Allow',
                  Action: [
                    'route53:ChangeResourceRecordSets',
                    'route53:ListResourceRecordSets'
                  ],
                  Resource: join!('arn:aws:route53:::hostedzone/', ref!(:hosted_zone_id))
                }
              ]
            }
          }
        ]
      end
    end
  end
end
