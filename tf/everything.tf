terraform {
  backend "s3" {
    key	   = "terraform.tfstate"
    bucket = "terraform-bucket-1xvxek8cgnfoi"
    region = "us-east-2"
  }
}

provider "aws" {
  region = "us-east-2"
}

data "aws_cloudformation_stack" "blog" {
  name = "blog"
}

resource "aws_s3_bucket" "blog" {
  bucket = "${data.aws_cloudformation_stack.blog.outputs.Bucket}"
  acl = ""
  website {
    index_document = "index.html"
    routing_rules = <<EOF
[{
    "Condition": {
	"KeyPrefixEquals": "our-new-sid-meiers-civilization-inspired-budget/"
    },
    "Redirect": {
	"Hostname": "www.alexrecker.com",
	"Protocol": "https",
	"ReplaceKeyWith": "civ-budget.html"
    }
},
{
    "Condition": {
	"KeyPrefixEquals": "using-selenium-buy-bus-pass/"
    },
    "Redirect": {
	"Hostname": "www.alexrecker.com",
	"Protocol": "https",
	"ReplaceKeyWith": "selenium-bus-pass.html"
    }
}]
EOF
  }
}

resource "aws_s3_bucket_policy" "blog" {
  bucket = "${data.aws_cloudformation_stack.blog.outputs.Bucket}"
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
	{
	    "Effect": "Allow",
	    "Principal": {
		"AWS": "arn:aws:iam::923035226446:user/${data.aws_cloudformation_stack.blog.outputs.Publisher}"
	    },
	    "Action": "s3:*",
	    "Resource": [
		"arn:aws:s3:::${data.aws_cloudformation_stack.blog.outputs.Bucket}",
		"arn:aws:s3:::${data.aws_cloudformation_stack.blog.outputs.Bucket}/*"
	    ]
	},
	{
	    "Effect": "Allow",
	    "Principal": "*",
	    "Action": "s3:GetObject",
	    "Resource": "arn:aws:s3:::blog-bucket-1edd2xs506ohe/*"
	}
    ]
}
POLICY
}
