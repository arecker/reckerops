#!/usr/bin/env bash

set -e

log() {
    echo "ci.sh: $1"
}

HERE="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

case "$1" in
    cloudformation)
	log "moving to $HERE/cfn"
	cd "$HERE/cfn"
	log "installing gems"
	bundle install
	log "validating stacks"
	bundle exec stack_master validate
	log "applying stacks"
	bundle exec stack_master apply --yes
	;;
    terraform)
	log "moving to $HERE/tf"
	cd "$HERE/tf"
	log "initializing terraform"
	terraform init
	log "generating terraform plan"
	terraform apply -auto-approve
	;;
    *)
	log "invalid option $1"
	exit 1
	;;
esac
